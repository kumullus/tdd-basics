# Kata - TDD basics

## Enoncé

Le but de ce kata est de comprendre le principe de Test Driven Development (ou TDD).
La feature que l'on veut tester est la suivante: *On veut calculer la différence
entre la somme des nombres de 0 à N au carré et la somme de chaque nombre au carré entre 0 et N*

exemple pour N = 5 : (1+2+3+4+5)² - (1² + 2² + 3² + 4² + 5²) = 225 - 55 = 170

## Principe des TDD

Pour coder une nouvelle feature, il est nécessaire d'utiliser le principe de
**Red-Green-Refactor**:
  1. Ecrire sa fonction de test
  2. Lancer le test et le voir fail
  3. Ecrire le code qui répond exactement à la fonction de test ecrite précedemment
  4. Lancer le test et le voir réussir
  5. Refactorer le code si besoin
  6. Itérer et recommencer à partir de l'étape n°1
